'''
with open("advent.txt") as f:
    xs=f.read().strip().split("\n")

xs=[(x.split(" ")) for x in xs]
xs=[(x[0].split("-"), x[1],x[2]) for x in xs]

c=0
def f(min1,max1, letter, str1):
    global c 
    s=str1.count(letter)
    if min1<=s<=max1:
       c=c+1 
    
        
for x in xs:
    min1=int(x[0][0])
    max1=int(x[0][1])
    letter=x[1][0]
    str1=x[2]
    f(min1,max1,letter,str1)
print(c)
'''


with open("advent.txt") as f:
    xs=f.read().strip().split("\n")

xs=[(x.split(" ")) for x in xs]
xs=[(x[0].split("-"), x[1],x[2]) for x in xs]

c=0
def f(min1,max1, letter, str1):
    global c
    s=0
    if str1[min1-1]==letter:
       s=s+1
    if max1<len(str1)+1 and str1[max1-1]==letter :
        s=s+1
    return s
    
    
        
for x in xs:
    min1=int(x[0][0])
    max1=int(x[0][1])
    letter=x[1][0]
    str1=x[2]
    s=f(min1,max1,letter,str1)
    if s==1:
        c=c+1
    
print(c)
